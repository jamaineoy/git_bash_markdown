# BASH and Markdown 101

This repository is our documentation on bash and markdown. We will put here the main commands of basg and how to document it in markdown.

Unordered points:
- something
- like
- this

Ordered list:
1. something
2. like
3. this

## BASH
Bash is a language that speaks with the Kernel. Used throughout linux machines (90%)

### Main commands
Write here the **`main commands`**

#### Where am I?
    $ pwd
    > /user/path/location

#### Where can I go?
Short list:

    $ ls
    > dir_a  dir_b  dir_c

##### Short list all (shows hidden directories):
    $ls
    > dir_a dir_b dir_c .hid_a .hid_b

Long list:
    
    $ ll

    > total 8
    > <permissions> <no. of linked hard links> <file owner> <group which file belongs to> <size> <date> <filename>

    $ ll -a


#### Go somewhere
    $ cd <path/directory>
    > ~/<path/directory>/

#### Come back
    $ cd ..
    > ~

#### play creationist
    $ touch <path> <path>

#### Create a folder/directory 
    $ mkdir <new dir>

    $ ls
    > <new dir>

#### remove a file
    $ rm <path/target>

#### remove a directory
    $ rmdir <path/dir/>

    $ rm -rf <path/dir/>

#### Printing to console
    $ echo 'hello'
    > hello

#### truncate output to file
    $ echo 'hello' > example.file
    $ cat example.file
    > hello

#### append output to file
    $ echo 'my name is' >> example.file
    $ cat example.file
    > hello
    > my name is
