# **Environment Variables in BASH** 

In this class we looked at Variables in bash. 
Included: 
 
## First Exercise - Setting a variable in memomory
The class included:
 - setting a variable using command line `NAME=JAMAINE`
 - Calling a variable `echo $NAME`
 - We logged out of our session in bash and logged back in
 - Variable was no longer ser
 - 


## Setting a variable in `.bash_profile`

```BASH
export NAME=JAMAINE
export BOOK=THE_FLASH
```


## Making a File that calls variable (Child process)

in a file called `files`
```BASH
#!/bin/bash
echo $NAME
echo $BOOK
echo $DRINK
echo 'Hello from files'
```

## Morphing the variable using export inside the file, and calling another file

```BASH
#!/bin/bash
$ cat files
echo $NAME
echo $BOOK
echo $DRINK
echo 'Hello from files'

export DRINK=GIN
~/hello
```

## Setting aliases/functions in `.bashrc`
```BASH
alias greet='echo 'Hello' $LOGNAME'

# changes user terminal to current directory and display the terminal line number.
PS1='$PWD[$LINENO]> '
```

## Setting user permissions
### User Type
| Syntax | Meaning |
| :-- | :-- |
| u | user |
| g | group |
| o | other |

<br>

### Permissions
| Syntax | Meaning |
| :-- | :-- |
| r | read |
| w | write |
| x | execute |
| s | special bit |
| t | sticky bit |

```BASH
# adding permissions
chmod <usertype>+<permission> <filename>

# removing permissions
chmod <usertype>-<permission> <filename>
```