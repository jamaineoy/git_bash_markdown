# **OS Fundamentals: Commonly Used Commands & Wildcards**

## **Commonly Used Commands**
### **User Information**
| Syntax | Meaning |
|:---| :---|
| id   | who you are currently working as |
| who | who is currently logged on  |
| last | who was or currently on the system  |
| date | current time for the system<br>can be used to change date of the system/format date |
---

### **Finding and getting to files**
| Syntax | Meaning |
|:---| :---|
| cd | Change directory, covered in the previous module |
| pwd | Where i am in the file system  |
| ls | List the contents of directories<br>Provide more information about file and directory attributes  |
| file | Identify the type of file<br>Where Windows uses .exe, .com, .dll<br>How does Unix/Linux identify what the type of file is? |
| which | Tell me where a command is coming from using directory locations defined by my environment  |
| whereis | Tell me where a command is based on a predefined set of directories  |
| find | Search for files or directories in the file system<br>Syntax<ul><li>find list_of_starting_directories_spaced [arguments] [actions]</li><li>Arguments are also referred to as predicates<li></li></li> </ul> |
| df | <li>Tell me how much of the file system(s) is in use <li>Tell me how the file system structure is made up|
| du | Tell me which directories are using how much space |
| lsof | Tells me which processes are using what files and vice versa |
---

### **File Manipulation**
All the commands below can make use of the **-r** option if you are intending to manipulate directories. If mistake is made, recovery can only be done from a backup.<br>
The **-i** option can be used to prevent you from overwriting a file of the same name in your destination.

| Syntax | Meaning |
|:---| :---|
| touch <'filename1'> | Create an empty file<br>Update the time stamps on a file or directory |
| cp | Copy files or directories. e.g. cp source1 source2 .... **directory**  |
| mv | Renames (moves) files or directory. e.g. mv source1 source2 ... **directory**  |
| rm | Remove files or directories. e.g. rm source 1 [source2 ... sourceN]|
| cat | Concatenate and print file |
---

### **Directory Manipulation**
| Syntax | Meaning |
|:---|:---|
| rmdir | Remove only empty directories |
| rm -rf | Removes directories |
| mkdir | Create directories. The **-p** option can be used to forced to create child directories |
---

### **Solutions**
Check you are logged in
```BASH
$ whoami
$ id
```

<br>
Create directory structure in one command

```BASH
$ mkdir -p {books,music,movies} books/{fiction,non-fiction}
```

<br>
Find out where the passwd command is and copy that file into movies/comedy/standup

```BASH
$ cp $(which passwd) movies/comedy/standup
```
<br>
Locate all the files in the /usr/bin and the /etc directory that begin with a p.

```BASH
$ find /usr/bin /etc -name 'p*'
```
<br>
Find files in teh operating system that have a modification date less than a day old. (-'n': used for less than, '+n': used for more than).

```BASH
$ find / -mtime -1
```

---
## **WildCards**
|Syntax|Meaning|
|:--|:--|
| * | Represents zero or more characters |
| ? | Represents a single character |
| [] | Represents a range of a characters (*) |

(*) --- **!** can be used as NOT inside the brackets.

