# Documenting GIT & BitBucket

## Intro
GIT is a version control software.

## Commands

```
# Start repo
$ git init

# Add files
$ git add . # adds all files

# Add remote
$ git remote add <bitbucket_repo>

# Commit with message
$ git commit -m "descriptive message"

# Check repo status
$ git status

# Check log of git commits
$ git log

# Change branch
$ git checkout -b <branch_name>
```